import { REGISTER } from '../constants';

const initialState = {
    success: false,
    loading: false,
    error: {
        triggered: false,
        message: ''
    }
};

export default (state = initialState, action) => {
    switch(action.type) {
        case(REGISTER.REGISTER_SUCCESS):
            return {
                ...state,
                success: true,
                loading: false,
                error: {
                    triggered: false,
                    message: ''
                }
            };
        
        case(REGISTER.REGISTER_FAILED):
            return {
                ...state,
                success: false,
                loading: false,
                error: {
                    triggered: true,
                    message: action.payload
                }
            };
        
        case(REGISTER.REGISTER_LOADING):
            return {
                ...state,
                loading: true
            };

        default: 
            return state;
    }
};