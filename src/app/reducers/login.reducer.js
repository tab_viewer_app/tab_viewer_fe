import { LOGIN } from '../constants';

const initialState = {
    loading: false,
    user: {
        logged: false,
        info: {}
    },
    error: {
        triggered: false,
        message: ''
    }
};

export default (state = initialState, action) => {
    switch(action.type) {
        case(LOGIN.LOGIN_SUCCESS):
            return {
                ...state,
                loading: false,
                user: {
                    logged: true,
                    token: action.payload.token,
                    info: action.payload.user
                },
                error: {
                    triggered: false,
                    message: ''
                }
            };
        
        case(LOGIN.LOGIN_FAILED):
            return {
                ...state,
                loading: false,
                error: {
                    triggered: true,
                    message: action.payload
                }
            };
        
        case(LOGIN.LOGIN_LOADING):
            return {
                ...state,
                loading: true
            };

        case(LOGIN.LOGOUT):
            return {
                ...state,
                user: {
                    logged: false,
                    info: {}
                }
            }

        default: 
            return state;
    }
};