import { combineReducers } from 'redux'
import loginState from './login.reducer';
import registerState from './register.reducer';

export default combineReducers({
    loginState,
    registerState
});