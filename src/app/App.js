import React, { Component } from 'react';
import { Provider } from 'react-redux';

import Header from './components/Header/Header';
import AppRouter from './AppRouter';
import Footer from './components/Footer/Footer';
import { store } from './store';

import './App.css'; 

class App extends Component {
	render() {
		return (
			<Provider store={ store }>
				<div>
					<Header />
					<AppRouter />
					<Footer />
				</div>
			</Provider>
		);
	}
}

export default App;
