import { REGISTER } from '../constants';

export const register = (user) => async dispatch => {
    dispatch(loading());
    try{
        const action = await registerAttempt(user);
        dispatch(action);
    }catch(err){
        dispatch(failed(err));
    }
}

const registerAttempt = (user) => {
    return new Promise( (resolve, reject) => {
        fetch('/account/register', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(user)
        })
        .then( res => res.json())
        .then( res => {
            if(res.success){
                resolve ({
                    type: REGISTER.REGISTER_SUCCESS,
                    payload: res.payload
                });
            }
            reject(new Error(res.payload.message));
        })
        .catch( err => {
            reject(err);
        })
    })
};

const loading = () => {
    return {
        type: REGISTER.REGISTER_LOADING
    }
}

const failed = (err) => {
    return {
        type: REGISTER.REGISTER_FAILED,
        payload: err.message
    }
}