import { LOGIN } from '../constants';

const loginAttempt = (email, password) => {
    return new Promise( (resolve, reject) => {
        fetch('/account/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ email, password })
        })
        .then( res => res.json())
        .then( res => {
            if(res.success){
                resolve ({
                    type: LOGIN.LOGIN_SUCCESS,
                    payload: res.payload
                });
            }
            reject(new Error(res.payload.message));
        })
        .catch( err => {
            reject(err);
        })
    })
};

const loading = () => {
    return {
        type: LOGIN.LOGIN_LOADING
    }
}

const failed = (err) => {
    return {
        type: LOGIN.LOGIN_FAILED,
        payload: err.message
    }
}

export const login = (email,password) => async dispatch => {
    dispatch(loading());

    try {
        const action = await loginAttempt(email,password);
        dispatch(action);
    } catch (err) {
        dispatch(failed(err));
    } 
}

export const logout = () => async dispatch => {
    dispatch({
        type: LOGIN.LOGOUT
    });
}