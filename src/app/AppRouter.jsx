import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import HomePage from './components/HomePage/HomePage';
import About from './components/About/About';
import Login from './components/Login/Login';
import Register from './components/Register/Register';

class AppRouter extends Component {
    render() {
        return (
            <div className="main">
                <Switch>
                    <Route exact path="/" component={HomePage} />
                    <Route path="/login" component={Login} />
                    <Route path="/about" component={About} />
                    <Route path="/register" component={Register} />
                </Switch>
            </div>
        );
    }
}

export default AppRouter;
