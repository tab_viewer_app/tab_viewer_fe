import React, { Component } from 'react';
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { register } from '../../actions/register.action';
import { connect } from 'react-redux';

class Register extends Component {
    constructor(props){
        super(props);

        this.state = {
            accountname: '',
            username: '',
            email: '',
            password: ''
        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.register(this.state);
    }

    componentDidUpdate = () => {
        if(this.props.registerState.success)
            this.props.history.replace('/login');
    }

    render() {
        return (
            <div className="form">
                <h1>Register</h1>
                <hr />
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup row>
                        <Label sm={3}>Username</Label>
                        <Col sm={9}>
                            <Input 
                            required 
                            type="text" 
                            name="username"
                            onChange={this.onChange} 
                            placeholder="Unique name used for login" />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={3}>Name</Label>
                        <Col sm={9}>
                            <Input 
                            required 
                            type="text"
                            name="accountname"
                            onChange={this.onChange}  
                            placeholder="Full name user for presentation" />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={3}>E-mail</Label>
                        <Col sm={9}>
                            <Input 
                            required 
                            type="email" 
                            name="email"
                            onChange={this.onChange} 
                            placeholder="example@email.com" />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={3}>Password</Label>
                        <Col sm={9}>
                            <Input required 
                            type="password" 
                            name="password"
                            onChange={this.onChange}  
                            placeholder="Insert your password" 
                            minLength={8}/>
                        </Col>
                    </FormGroup>
                    <Button className="btn-login">Submit</Button>
                </Form>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { 
        registerState: state.registerState,
    };
};

export default connect(mapStateToProps, { 
    register
})(Register);