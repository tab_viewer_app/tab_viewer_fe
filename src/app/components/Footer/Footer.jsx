import React, { Component } from 'react';

import linkedin from '../../assets/linkedin.png';
import gmail from '../../assets/gmail.png';

import './Footer.css';

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <div className="logos">
                    <ul>
                        <li>
                            <a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/tiago-ferreira-52b630a4/">
                                <img src={linkedin} alt="Author LinkedIn" />
                            </a>  
                        </li>
                        <li>
                            <a target="_blank" rel="noopener noreferrer" href="https://mail.google.com/mail/?view=cm&fs=1&to=tiagomnferreira@gmail.com">
                                <img src={gmail} alt="Author Gmail"/>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Footer;
