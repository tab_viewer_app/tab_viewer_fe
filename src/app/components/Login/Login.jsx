import React, { Component } from 'react';
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { login } from '../../actions/login.action';
import { connect } from 'react-redux';

import './Login.css';

class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email : '',
            password: ''
        };
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    /**
     * Calls the login action and sends it the username and password in the current state of the component
     * @param {*} e 
     */
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.login(
            this.state.email,
            this.state.password
        );
    }

    componentDidUpdate = () => {
        if(this.props.loginState.user.logged) 
            this.props.history.replace('/');
    }

    render() {
        return (
            <div className="form">
                <h1>Login</h1>
                <hr />
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup row>
                        <Label sm={3}>Email</Label>
                        <Col sm={9}>
                            <Input 
                            type="text" 
                            name="email"
                            onChange={this.onChange} 
                            placeholder="Insert your email" />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={3}>Password</Label>
                        <Col sm={9}>
                            <Input 
                            type="password" 
                            name="password" 
                            onChange={this.onChange} 
                            placeholder="********" />
                        </Col>
                    </FormGroup>
                    <Button className="btn-login">Submit</Button>
                </Form>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { 
        loginState: state.loginState,
    };
};

export default connect(mapStateToProps, { 
    login
})(Login);