import React, { Component } from 'react';
import './HomePage.css';

import { Jumbotron, Button } from 'react-bootstrap';

class HomePage extends Component {
    render() {
        return (
            <div>
                <Jumbotron>
                    <center>
                        <h1>Home Page!</h1>
                        <p>
                            This is the first home page developed for the Tab Viewer App
                        </p>
                        <Button bsSize="large">Get Started</Button>
                    </center>
                </Jumbotron>
            </div>
        )
    }
}

export default HomePage;