import React, { Component } from 'react';
import { Navbar, NavbarBrand, Nav, NavItem } from 'reactstrap';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { logout } from '../../actions/login.action'; 

import homeLogo from '../../assets/home.png';

import './Header.css';

class Header extends Component {

    logout = (e) => {
        e.preventDefault();
        this.props.logout();
    }

    render() {
        return (
            <div className="header">
                <Navbar light expand="md">
                    <NavbarBrand className="nav-brand" href="/">TabViewer</NavbarBrand>
                    <Nav className="nav">
                        {
                            this.props.loginState.user.logged ?
                            <div className="nav-left">
                                <NavItem>
                                    <Link className="nav-link" to="/login">Settings</Link>
                                </NavItem>
                                <NavItem>
                                    <Link className="nav-link" to="/register" onClick={this.logout}>Logout</Link>
                                </NavItem>
                            </div>
                            :
                            <div className="nav-left">
                                <NavItem>
                                    <Link className="nav-link" to="/register">Register</Link>
                                </NavItem>
                                <NavItem>
                                    <Link className="nav-link" to="/login">Login</Link>
                                </NavItem>
                            </div>
                        }
                        <div className="nav-mid">
                            <Link to="/">
                                <img src={homeLogo} alt="HomePage"/>
                            </Link>
                        </div>
                        <div className="nav-right">
                            <NavItem>
                                <Link className="nav-link" to="/about">About</Link>
                            </NavItem>
                        </div>
                    </Nav>
                </Navbar>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        loginState: state.loginState,
        register: state.register
    };
};

export default connect(mapStateToProps,{
    logout
})(Header);